#pragma once

#include <vector>

#include "Input.h"
#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "SpriteFont.h"

namespace GC
{
	const float ASTEROID_SPEED = 100;
	const float ASTEROID_SPAWN_RATE = 1;
	const float ASTEROID_SPAWN_INC = 1;
	const float ASTEROID_MAX_SPAWN_DELAY = 10;
	const int ROID_CACHE = 32;
}


struct Bullet
{
	Bullet(MyD3D& d3d)
		:bullet(d3d)
	{}
	Sprite bullet;
	bool active = false;

	void Init(MyD3D& d3d);
	void Render(DirectX::SpriteBatch& batch);
	void Update(float dTime);
	const float MISSILE_SPEED = 300;
};

/*
Animated asteroid
*/
struct Asteroid
{
	Asteroid(MyD3D& d3d)
		:spr(d3d)
	{}
	Sprite spr;
	bool active = false;	//should it render and animate?

	//setup
	void Init();
	/*
	draw - the atlas has two asteroids, half frames in one, half the other
	asteroids are randomly assigned one and then animate and at random fps
	*/
	void Render(DirectX::SpriteBatch& batch);
	/*
	move left until offscreen, then go inactive
	*/
	void Update(float dTime);
};

//--------Score class
class Score
{
public:
  void init();
  int getAmount() const;
  void updateAmount(int);
private:
  int amount_;
};
//--------end of Score class



//horizontal scrolling with player controlled ship
class PlayMode
{
public:
	PlayMode(MyD3D& d3d);
	void Update(float dTime);
	void Render(float dTime, DirectX::SpriteBatch& batch, DirectX::SpriteFont& font);
  bool dead = false;
  Score score;

private:
	const float SCROLL_SPEED = 10.f;
  static const int BGND_LAYERS = 8;
	const float SPEED = 250;
	const float MOUSE_SPEED = 5000;
	const float PAD_SPEED = 500;
  

	MyD3D& mD3D;
	std::vector<Sprite> mBgnd; //parallax layers
	RECTF mPlayArea;	//don't go outside this	
	Sprite mPlayer;		//jet
	Sprite mThrust;		//flames out the back
	Bullet mMissile;	//weapon, only one at once
  Sprite mCoinSpin;

  void InitPlayer();
  void InitScore();
  void InitBgnd();
  
 
  

	//Asteroid Stuff
	std::vector<Asteroid> mAsteroids;
	float SpawnRateSec = GC::ASTEROID_SPAWN_RATE;
	float lastSpawn = 0;
	Asteroid* spawnAsteroid();
	void updateAsteroids(float dTime);
	void initAsteroids();
	void renderAsteroids(DirectX::SpriteBatch & batch);
	Asteroid *checkCollAsteroids(Asteroid& me);
	
	//once we start thrusting we have to keep doing it for 
	//at least a fraction of a second or it looks whack
	float mThrusting = 0; 



	//make it move, reset it once it leaves the screen, only one at once
	void UpdateMissile(float dTime);
	//make it scroll parallax
	void UpdateBgnd(float dTime);
	//move the ship by keyboard, gamepad or mouse
	void UpdateInput(float dTime);
	//make the flames wobble when the ship moves
	void UpdateThrust(float dTime);

  void UpdateText(DirectX::SpriteBatch& batch, DirectX::SpriteFont& font);
};

class IntroMode
{
public:
  IntroMode(MyD3D& d3d);
  void Render(float dTime, DirectX::SpriteBatch& batch, DirectX::SpriteFont& font);
  
  void Init();
private:
  static const int BGND_LAYERS = 8;
  
  MyD3D& mD3D;
  std::vector<Sprite> mBgnd; //parallax layers

};

class GOverMode
{
public:
  GOverMode(MyD3D& d3d);
  void Render(float dTime, DirectX::SpriteBatch& batch, DirectX::SpriteFont& font, int & s);
  void UpdateText(DirectX::SpriteBatch& batch, DirectX::SpriteFont& font, int & s);
  void Init();
private:
  static const int BGND_LAYERS = 8;

  MyD3D& mD3D;
  std::vector<Sprite> mBgnd; //parallax layers

};



/*
Basic wrapper for a game
*/
class Game
{
public:
	enum class State { PLAY, START, GAME_OVER };
	static MouseAndKeys sMKIn;
	static Gamepads sGamepads;
	State state = State::START;
	Game(MyD3D& d3d);
  


	void Release();
	void Update(float dTime);
	void Render(float dTime);

private:
	MyD3D& mD3D;
	DirectX::SpriteBatch *mpSB = nullptr;
  DirectX::SpriteFont *mpF = nullptr;
	//not much of a game, but this is it 
  IntroMode mIMode;
	PlayMode mPMode;
  GOverMode mGOMode;
};

 
